#!/usr/bin/env bash
#set -evx # Quit script on error


amixer -D hw:0 set Master mute

jack_wait -w > /dev/null 2>&1

pulse_connections=$(jack_lsp -c | awk -F "    " '{print $1 $2 $3}'|grep "  "|grep "Pulse")

refresh_patchage() {
  xdotool search --onlyvisible --name "Patchage" key ctrl+r
  sleep 0.25
  xdotool search --onlyvisible --name "Patchage" key ctrl+g
  sleep 0.25
  xdotool search --onlyvisible --name "Patchage" key ctrl+f
}

connect_pulse() {
  jack_connect "PulseAudio JACK Sink:front-left" "zita-mu1:in_1.L"
  jack_connect "PulseAudio JACK Sink:front-right" "zita-mu1:in_1.R"

  jack_connect "zita-mu1:mon_out1.L" "system:playback_1"
  jack_connect "zita-mu1:mon_out1.R" "system:playback_2"
}

disconnect_pulse() {
  jack_disconnect "PulseAudio JACK Sink:front-left" system:playback_1
  jack_disconnect "PulseAudio JACK Sink:front-right" system:playback_2

  jack_disconnect "PulseAudio JACK Source:front-left" system:capture_1
  jack_disconnect "PulseAudio JACK Source:front-right" system:capture_2
}

if [[ -z "$pulse_connections" ]];
then
  systemd-cat -t "soundcheck" echo "Pulse is not connected to any jack ports"
else
  disconnect_pulse
fi

i3-msg "workspace 5"

if ! pgrep -x "zita-mu1" > /dev/null
then
  i3-msg "exec --no-startup-id zita-mu1"
  sleep 0.25
  connect_pulse
fi

if ! pgrep -x "patchage" > /dev/null;then
  i3-msg "exec --no-startup-id patchage"
  sleep 0.25
fi

i3-msg "exec --no-startup-id pavucontrol"

i3-msg "exec --no-startup-id uxterm -class 'alsamixer' -e alsamixer -c 1"

i3-msg "exec --no-startup-id uxterm -class 'dialog' -e dialog \
--msgbox 'check amp\n\nmixer levels\n\nlook behind you' \
10 40"

menu() {

  action="$(dialog --clear \
            --menu "What do you want to do?" 10 40 5 \
            "soundcheck" "make sure sound is working" \
            "backup" "backup" \
            "reboot" "backup and reboot" \
            "shutdown" "backup and shutdown" 2>&1 >/dev/tty)"
}

soundcheck() {

  play -V -r 48000 -n synth 1 sin 440 vol -20dB; sleep 0.25

  uxterm -class 'dialog' -e dialog --clear --title "Title"  --yesno "Hear anything?" 0 0 2>&1 >/dev/tty

  echo $?

  if [ $? = 0 ]; then
    echo "great!"
  else
    echo "let the troubleshooting begin"
  fi
}

# if an argument is passed to the script
# then assign the action variable to that argument
# if nothing is passed then load the menu

if [ "${1-nothing}" = "nothing" ]; then
  menu
else
  action="$1"
fi

# if there is no argument passed or the menu is cancelled
# then polietly exit

if [[ -z "$action" ]]; then
  echo "nevermind then"
  sleep 1
  exit
fi

if [ "$action" = "soundcheck" ]; then

  echo "running soundcheck..."

  soundcheck

  if [ $? = 0 ]; then
    echo "gooood"
  else
    echo "soundcheck was not run, checklog"
    exit
  fi

elif [ "$action" = "backup" ]

then
  echo "well, you get the idea..."
fi

sleep 0.25

if [ $? = 0 ]; then
  i3-msg "[class="^Patchage$"], floating enable, resize set 1254 782, move position 1483px 1127";sleep 0.25

  i3-msg "[class="^Pavucontrol$"], resize set 1103 535, move position 2829px 1328";sleep 0.25

  i3-msg "[class="alsamixer"], floating enable, resize set 1319 479, move position 2616px 585";sleep 0.25

  i3-msg "[class="Zita-mu1"], scratchpad show, move position 2891px 1168";sleep 0.25

  i3-msg "[class="dialog"], move position 1739px 580, focus";sleep 0.25

  refresh_patchage

  amixer -D hw:0 set Master unmute
else
  i3-msg "exec --no-startup-id uxterm -class 'dialog' -e dialog \
        --msgbox 'Something terrible has happened...\n\ncheck $loge\n\nlook behind you' \
        10 40"
  sleep 1
  exit
fi
